from datetime import datetime
from config import db

class Income(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    source = db.Column(db.String(100), nullable=False)
    amount = db.Column(db.Float, nullable=False)
    date_received = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    category = db.Column(db.String(100), nullable=False)


    def __repr__(self):
        return f"Income('{self.source}', '{self.amount}', '{self.date_received}')"
