from datetime import datetime
from config import db  

class Expense(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(100), nullable=False)
    amount = db.Column(db.Float, nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    category = db.Column(db.String(100), nullable=False)


    def __repr__(self):
        return f"Expense('{self.description}', '{self.amount}', '{self.date_posted}')"
