#sys imports
from datetime import datetime

#flask stuff
from flask import render_template, request, redirect, url_for

# app instance and config options
from config import app, db

# database models
from models.expense import Expense
from models.income import Income
from models.budget import Budget


@app.route('/')
def index():
    current_year = datetime.now().year
    current_month = datetime.now().month
    total_expenses_this_month = get_total_for_month(Expense, Expense.date_posted, current_year, current_month)
    total_income_this_month = get_total_for_month(Income, Income.date_received, current_year, current_month)

    expenses_by_category = db.session.query(
        Expense.category,
        db.func.sum(Expense.amount)
    ).group_by(Expense.category).all()

    income_by_category = db.session.query(
        Income.category,
        db.func.sum(Income.amount)
    ).group_by(Income.category).all()

    return render_template(
        'index.html', 
        total_expenses=total_expenses_this_month,
        total_income=total_income_this_month,
        expenses_by_category=expenses_by_category,
        income_by_category=income_by_category
    )
@app.route('/expenses')
def expenses():
    months = {1: "January", 2: "February", 3: "March", 4: "April", 5: "May", 6: "June",
              7: "July", 8: "August", 9: "September", 10: "October", 11: "November", 12: "December"}
    years = list(range(2020, datetime.now().year + 1))
    categories_list = ["Groceries", "Bills","Entertainment", "Miscellaneous"]
    # Fetch the selected month and year or default to the current month and year
    selected_month = request.args.get('month', default=datetime.now().month, type=int)
    selected_year = request.args.get('year', default=datetime.now().year, type=int)

    all_expenses = Expense.query.filter(
        db.extract('year', Expense.date_posted) == selected_year,
        db.extract('month', Expense.date_posted) == selected_month
    ).all()

    return render_template('expenses.html', expenses=all_expenses, months=months.items(), years=years, 
                           selected_month=selected_month, selected_year=selected_year, categories=categories_list)

@app.route('/add_expense', methods=['GET', 'POST'])
def add_expense():
    if request.method == 'POST':
        description = request.form['description']
        amount = float(request.form['amount'])
        category = request.form['category']

        new_expense = Expense(description=description, amount=amount, category=category)
        db.session.add(new_expense)
        db.session.commit()

        return redirect(url_for('expenses'))
    return render_template('expenses.html')

@app.route('/delete_expenses', methods=['POST'])
def delete_expenses():
    selected_ids = request.form.getlist('selected_expenses')
    for expense_id in selected_ids:
        expense_to_delete = Expense.query.get(expense_id)
        if expense_to_delete:
            db.session.delete(expense_to_delete)
    db.session.commit()
    return redirect(url_for('expenses'))

@app.route('/income')
def income():
    months = {1: "January", 2: "February", 3: "March", 4: "April", 5: "May", 6: "June",
              7: "July", 8: "August", 9: "September", 10: "October", 11: "November", 12: "December"}

    years = list(range(2020, datetime.now().year + 1))

    categories_list = ["Salary", "Side-Hustle", "One-Off"]

    selected_month = request.args.get('month', default=datetime.now().month, type=int)
    selected_year = request.args.get('year', default=datetime.now().year, type=int)

    all_income = Income.query.filter(
        db.extract('year', Income.date_received) == selected_year,
        db.extract('month', Income.date_received) == selected_month
    ).all()

    return render_template('income.html', incomes=all_income, expense=expenses, months=months.items(), years=years,
                           selected_month=selected_month, selected_year=selected_year, categories=categories_list)

@app.route('/add_income', methods=['GET', 'POST'])
def add_income():
    if request.method == 'POST':
        source = request.form['source']
        amount = float(request.form['amount'])
        category = request.form['category']

        new_income = Income(source=source, amount=amount, category=category)
        db.session.add(new_income)
        db.session.commit()

        return redirect(url_for('income'))
    return render_template('income.html')

@app.route('/delete_income', methods=['POST'])
def delete_income():
    selected_ids = request.form.getlist('selected_incomes')
    for income_id in selected_ids:
        income_to_delete = db.session.get(Income, income_id)
        if income_to_delete:
            db.session.delete(income_to_delete)
    db.session.commit()
    return redirect(url_for('income'))

@app.route('/budget')
def budget():
    all_budgets = Budget.query.all()  # Assuming you have a Budget model
    categories_list = ["Groceries", "Bills", "Miscellaneous", "Entertainment"]   # Adjust as needed
    return render_template('budget.html', budgets=all_budgets, categories=categories_list)

@app.route('/add_budget', methods=['GET', 'POST'])
def add_budget():
    if request.method == 'POST':
        category = request.form['category']
        limit_amount = float(request.form['limit_amount'])

        new_budget = Budget(category=category, limit_amount=limit_amount)
        db.session.add(new_budget)
        db.session.commit()

        return redirect(url_for('budget'))

    return render_template('budget.html')

def get_total_income():
    return db.session.query(db.func.sum(Income.amount)).scalar()

def get_total_for_month(model, date_field, year, month):
    return db.session.query(db.func.sum(model.amount)).filter(
        db.extract('year', date_field) == year,
        db.extract('month', date_field) == month
    ).scalar() or 0

if __name__ == '__main__':
    with app.app_context():
        # create tables if they don't exist already
        db.create_all()
    app.run(debug=True)
