# Personal Finance Tracker

## Overview

Personal Finance Tracker is a Flask-based web application designed to help users manage their personal finances. It allows tracking of expenses and income, categorizing them, and viewing monthly financial summaries.

## Features

- Add, view, and delete income and expenses.
- Categorize income and expenses into predefined categories.
- Filter records by month and year.
- View monthly total of income and expenses.

## Technologies

- Python
- Flask
- SQLAlchemy
- SQLite

## Installation

To set up the project locally, follow these steps:

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/melonware/personal-finances
    ```

2. Create a virtual environment and activate it

    ```bash
    python -m venv venv
    source venv/bin/activate
    ```

3. Install required dependencies:

    ```bash
    pip install -r requirements.txt
    ```

4. Run the application

    ```bash
    python src/app.py
    ```
